/**
 * Created by Invia on 29.03.2017.
 */
// Подсказка как играть
$(document).ready(function(){
    $("#myPopover").popover({
        title: 'Заголовок панели',
        content: 'Текст панели',
        trigger: 'hover',
        placement: 'right'
    });
});

// Анимация звезд
/*(function($){

    function generateStar(canvas, ctx, starRadius){
        ctx.beginPath();
        ctx.arc(starRadius + (Math.random() * canvas.width), starRadius + (Math.random() * canvas.height), starRadius*Math.random(), 0, Math.PI*2, false);
        //ctx.arc(100, 30, starRadius, 0, Math.PI*2, false);

        var rand = Math.random();
        if(rand <= 0.5){
            ctx.fillStyle = "rgba(255, 255, 255, 1)";
            ctx.shadowColor = "rgba(255, 255, 255, 0.5)";
            ctx.shadowBlur = 3;
        }
        else if(rand > 0.75){
            ctx.fillStyle = "rgba(255, 254, 196, 1)";
            ctx.shadowColor = "rgba(255, 254, 196, 0.5)";
            ctx.shadowBlur = 4;
        }
        else{
            ctx.fillStyle = "rgba(192, 247, 255, 1)";
            ctx.shadowColor = "rgba(192, 247, 255, 0.5)";
            ctx.shadowBlur = 7;
        }
        ctx.fill();
    }

    $(function(){

        var canvas = document.getElementById("spac");
        var context = canvas.getContext("2d");

        onresize = function(){
            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;
        }
        onresize();

        interval = setInterval(
            function(interval){
                generateStar(canvas, context, 3);
            },
            24);

        setTimeout( // Stop sreating stars after 10s
            function(){ clearInterval(interval); }
            ,1000
        );

    });
})(jQuery);*/



(function($){

    function generateStar(canvas, ctx, starRadius){
        ctx.beginPath();
        ctx.arc(starRadius + (Math.random() * canvas.width), starRadius + (Math.random() * canvas.height), starRadius*Math.random(), 0, Math.PI*2, false);
        //ctx.arc(100, 30, starRadius, 0, Math.PI*2, false);

        var rand = Math.random();
        if(rand <= 0.5){
            ctx.fillStyle = "rgba(255, 255, 255, 1)";
            ctx.shadowColor = "rgba(255, 255, 255, 0.5)";
            ctx.shadowBlur = 3;
        }
        else if(rand > 0.75){
            ctx.fillStyle = "rgba(255, 254, 196, 1)";
            ctx.shadowColor = "rgba(255, 254, 196, 0.5)";
            ctx.shadowBlur = 4;
        }
        else{
            ctx.fillStyle = "rgba(192, 247, 255, 1)";
            ctx.shadowColor = "rgba(192, 247, 255, 0.5)";
            ctx.shadowBlur = 7;
        }
        ctx.fill();
    }

    $(function(){

        var canvas = document.getElementById("space");
        var context = canvas.getContext("2d");

        onresize = function(){
            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;
        }
        onresize();

        interval = setInterval(
            function(interval){
                generateStar(canvas, context, 3);
            },
             24);

        setTimeout( // Stop sreating stars after 10s
            function(){ clearInterval(interval); }
            ,10000
        );

    });
    $(function(){

        var canvas = document.getElementById("spac");
        var context = canvas.getContext("2d");

        onresize = function(){
            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;
        }
        onresize();

        interval1 = setInterval(
            function(interval1){
                generateStar(canvas, context, 3);
            },
            24);

        setTimeout( // Stop sreating stars after 10s
            function(){ clearInterval(interval1); }
            ,100000
        );

    });
})(jQuery);


var radRandNumber = Math.floor(Math.random() * 9000) + 1000; //создаем случайное число


var substrRandNum = '' + radRandNumber; //делаем из числа строку


var firSubNum = +substrRandNum.substring(0, 1); //вьітягиваем из случайного числа первую цифру и снова приводим ее к чисельному типу данньіх

var secSubNum = +substrRandNum.substring(1, 2); //вьітягиваем из случайного числа вторую цифру и снова приводим ее к чисельному типу данньіх

var thiSubNum = +substrRandNum.substring(2, 3); //вьітягиваем из случайного числа третью цифру и снова приводим ее к чисельному типу данньіх

var fouSubNum = +substrRandNum.substring(3); //вьітягиваем из случайного числа четвертую цифру и снова приводим ее к чисельному типу данньіх

    function spaceGame() {
        document.getElementById("hack").innerHTML = substrRandNum;

        $("#buttonForSearch").attr('onClick', 'quest()'); //меняем функцию кнопке
        $("#buttonForSearch").text('Try again!'); //меняем подпись кнопки

        quest(); //запускаем функцию с парметрами вьітянутьіх случайньіх чисел
};

function quest() {
    var i = 0;
    if (i <= 10) {  //счетчик которьій определяет количество попьіток (по умолчанию 10)
        i++;
        var firstNumber = +document.getElementById("firstNumber").value;
        var secondNumber = +document.getElementById("secondNumber").value;
        var thirdNumber = +document.getElementById("thirdNumber").value;
        var fourthNumber = +document.getElementById("fourthNumber").value;

        first(firstNumber);
        second(secondNumber);
        third(thirdNumber);
        fourth(fourthNumber);

        if (firsRight & secRight & thirRight & fouRight == 1) { //если ==1 значит пользователь отгадал все числа
            return found();
        } else if (i > 9) { // если больше 9(количество попьіток (по умолчанию 10)) значит пользовтель проиграл
            return notfound();
        }


        function first(aUser) { //фунция сверки случайного первого числа и первого числа пользовтеля
            if (firSubNum == aUser) { //если случайно число равно числу пользователя
                stylFirstRight();
                return firsRight = 1; // возращаем число 1
            }
            else {
                stylFirstWrong();
                return firsRight = 0;
            }
        };

        function second(bUser) {
            if (secSubNum == bUser) {
                stylSecondRight();
                return secRight = 1;
            }
            else {
                stylSecondWrong();
                return secRight = 0;
            }
        };

        function third(cUser) {
            if (thiSubNum == cUser) {
                stylThirdRight();
                return thirRight = 1;
            }
            else {
                stylThirdWrong();
                return thirRight = 0;
            }
        };
        function fourth(dUser) {
            if (fouSubNum == dUser) {
                stylFourthRight();
                return fouRight = 1;
            }
            else {
                stylFourthWrong();
                return fouRight = 0;
            }
            ;
        };
        // изменяем стили после результата
        function stylFirstRight() {
            $("#firstNumber").css("color", "darkgreen");
        };
        function stylSecondRight() {
            $("#secondNumber").css("color", "darkgreen");
        };
        function stylThirdRight() {
            $("#thirdNumber").css("color", "darkgreen");
        };
        function stylFourthRight() {
            $("#fourthNumber").css("color", "darkgreen");
        };
        function stylFirstWrong() {
            $("#firstNumber").css("color", "red");
        };
        function stylSecondWrong() {
            $("#secondNumber").css("color", "red");
        };
        function stylThirdWrong() {
            $("#thirdNumber").css("color", "red");
        };
        function stylFourthWrong() {
            $("#fourthNumber").css("color", "red");
        };
        // функция которая меняет стили если тьі вьіиграл

        // функция которая меняет стили если тьі проиграл
        function notfound() {
            $('#spac').css('visibility', 'visible');
            $('#bottomrock').css('display', 'inline-block');
            $('.rocket').css('display', 'none');
            $("#inputHidden").css('display', 'none');
        };
    };
};
function found() {
    $('#topStars').css('display', 'none');
    $('#spac').css('display', 'inline-block');
    $('#bottomrock').css('display', 'inline-block');
    $('.rocket').css('display', 'inline-block');
    $("#inputHidden").css('display', 'none');
};